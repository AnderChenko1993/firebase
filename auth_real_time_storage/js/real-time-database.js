var usersList = document.getElementById('usersList');
var nameInput = document.getElementById('nameInput');
var lastNameInput = document.getElementById('lastNameInput');
var ageInput = document.getElementById('ageInput');
var addButton = document.getElementById('addButton');

//Ao clicar no botão
addButton.addEventListener('click', function () {
    create(nameInput.value, ageInput.value, lastNameInput.value);
});

function create(name, age, lastName) {
    var data = {
        name: name,
        lastName: lastName,
        age: age,
    };

    return firebase.database().ref().child('users').push(data);
}

firebase.database().ref('users').on('value', function (snapshot) {
    usersList.innerHTML = '';
    snapshot.forEach(function (item)  {
        var li = document.createElement('li');
        li.appendChild(document.createTextNode(item.val().name + ' ' + item.val().lastName + ' : ' + item.val().age));
        usersList.appendChild(li);
    });
});